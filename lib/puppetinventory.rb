require 'rubygems'
require 'yaml'
require 'json'
require 'spreadsheet'

require_relative 'puppetrest.rb'
require 'puppetinventory/version'

module Puppetinventory
  class Inventory
    @quite
    HEADER_ROW = 0
    @puppetdb_api_url

    def initialize(puppetdb_api_url = 'http://172.22.78.40:8080/v2/', quite = false)
      @puppetdb_api_url = puppetdb_api_url
      @quite = quite
    end


    def get_nodes
      pr = PuppetRest.new(@puppetdb_api_url)
      nodes = Hash.new

      pr.get_puppetdb('nodes').each do |n|
        if(n['deactivated'] == nil)
          nodes[n['name']] = { :node_info => n }
        end
      end


      nodes_hash = Hash.new

      nodes.each do |k, v|
        name = k
        if !@quite
          p "Node: #{name}"
        end

        params_in = ['=', 'certname', name].to_json
        facts = pr.get_puppetdb("facts", params_in)

        nodes_hash[name] = PuppetRest.create_hash facts
      end

      if !@quite
        p "Will Process #{nodes_hash.count} nodes."
      end
      nodes_hash
    end

    def generate(facts, quite = false)
      @quite = quite
      nodes = get_nodes

      book = Spreadsheet::Workbook.new
      vmSheet = book.create_worksheet(:name => 'Target VMs')


      #Process the column headers
      vmSheet[0,0]
      column = HEADER_ROW
      facts.each do |key, fact|
        vmSheet[0,column] = fact[:name]
        fact[:length] = fact[:name].length

        column = column + 1
      end


      #Process the data rows
      row = 1
      nodes.each do |k, n|
        column = 0
        facts.each do |key, fact|
          if fact[:yield]
            value = fact[:yield].call(n[key.to_s], fact[:length], n)

            fact[:length] = value[:length]
            vmSheet[row,column] = value[:value]

          else
            if n[key.to_s].class == String
              vmSheet[row,column] = n[key.to_s].upcase
            else
              vmSheet[row,column] = n[key.to_s]
            end
            if (fact[:length] < n[key.to_s].length)
              fact[:length] = n[key.to_s].length
            end
          end

          column = column + 1
        end

        row = row + 1
      end

      #Format
      header = Spreadsheet::Format.new  :weight => :bold

      wrap = Spreadsheet::Format.new :text_wrap => true
      every_other = Spreadsheet::Format.new :text_wrap => true



      #Set the column widths
      column_count = 0
      facts.each do |key, fact|
        vmSheet.column(column_count).width = fact[:length] + 5

        column_count = column_count + 1
      end

      #Format the column headers
      facts.count.times do |c|
        vmSheet.row(HEADER_ROW).set_format(c, header)
      end

      #Format the rows
      row = 3

      nodes.count.times do |n|
        facts.count.times do |c|
          vmSheet.row(row).set_format(c, every_other)
          vmSheet.row(row - 1).set_format(c, wrap)
        end

        row = row + 2
      end

      return book
    end
  end
end
