require 'net/http'
require 'rest-client'
require 'json'

class PuppetRest
  @puppetdb_api_url

  def initialize(puppetdb_api_url)
    @puppetdb_api_url = puppetdb_api_url
  end

  def self.create_hash(hash)
    cleaned_hash = Hash.new

    hash.each do |v|
      cleaned_hash[v['name']] = v['value']
    end

    return cleaned_hash
  end

  def post_puppetdb(end_point, params = Hash.new)
    JSON.parse(RestClient.get "#{@puppetdb_api_url}/#{end_point}", params.to_json, :content_type => :json, :accept => :json)
  end

  def get_puppetdb(end_point, params_in = nil)
    if (params_in == nil)
      JSON.parse(RestClient.get "#{@puppetdb_api_url}/#{end_point}", {:accept => :json})
    else
      JSON.parse(RestClient.get "#{@puppetdb_api_url}/#{end_point}", {:params => { "query" => params_in }, :accept => :json})
    end
  end
end
