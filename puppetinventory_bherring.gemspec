# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'puppetinventory/version'

Gem::Specification.new do |spec|
  spec.name          = 'puppet-inventory'
  spec.version       = Puppetinventory::VERSION
  spec.authors       = ['Brad Herring']
  spec.email         = ['brad@bherville.com']
  spec.description   = 'Write a gem description'
  spec.summary       = 'Write a gem summary'
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.3'
  spec.add_development_dependency 'rake'
  spec.add_runtime_dependency 'rest-client'
  spec.add_runtime_dependency 'spreadsheet'
  spec.add_runtime_dependency 'json'
end
